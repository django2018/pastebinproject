#!/usr/bin/env python
# https://stackoverflow.com/questions/39723310/django-standalone-script

import os, sys, django
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_DIR) # here store is root folder (means parent - django app project)
os.environ["DJANGO_SETTINGS_MODULE"] = "pastebinproject.settings"
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "pastebinproject.settings")

django.setup()

import datetime
from django.conf import settings
from pastebin.models import Paste

today = datetime.date.today()
cutoff = (today - datetime.timedelta(days = settings.EXPIRY_DAYS))
Paste.objects.filter(timestamp__lt=cutoff).delete()
Django==1.9.6
Markdown==2.6.11
MySQL-python==1.2.5
Pillow==5.2.0
protobuf==3.6.0
six==1.11.0

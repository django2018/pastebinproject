from django.conf.urls import url
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView

from pastebin.models import Paste

import views

display_info = {'queryset': Paste.objects.all()}
create_info = {'model': Paste}

urlpatterns = [
    url(r'^$', views.PasteListView.as_view()),
    url(r'^(?P<pk>\d+)/$', views.PasteDetailView.as_view(), name='paste-detail'),
    url(r'^add/$', views.PasteCreate.as_view()),
]

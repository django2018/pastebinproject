from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView

from pastebin.models import Paste

# Create your views here.

class PasteListView(ListView):
    model = Paste

class PasteDetailView(DetailView):
    model = Paste
    
class PasteCreate(CreateView):
    model = Paste
    fields = ['content', 'title', 'syntax', 'poster']
    
    # def post(self, request, *args, **kwargs):
        # print request.POST
        # from django.http import HttpResponse
        # return HttpResponse('Hello')
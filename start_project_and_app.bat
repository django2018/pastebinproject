echo off
set path=C:\virtualenv\python2.7\Scripts
set pythonpath=C:\virtualenv\python2.7\Lib\site-packages
echo Choose function 
echo -----*-----
echo 1. startproject
echo 2. startapp
set /p choice=
echo enter name project or app
set /p name=
if %choice% == 1 set start=django-admin startproject %name%
if %choice% == 2 set start=django-admin startapp %name%
echo on
%start%
pause